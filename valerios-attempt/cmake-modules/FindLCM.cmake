INCLUDE(FindPackageHandleStandardArgs)

SET(LCM_IncludeSearchPaths
  ~/apps # for nuCore
  /usr/include/
  /usr/local/include/
  /opt/local/include
)

SET(LCM_LibrarySearchPaths
  ~/apps
  /usr/lib/
  /usr/local/lib/
  /opt/local/lib/
)

SET(LCM_NUM_MESSAGES "0")
set(LCM_MSGS_DIR lcm_msgs_gen) # folder where the headers are generated (inside build)

FIND_PATH(LCM_INCLUDE_DIR lcm/lcm.h
  PATHS ${LCM_IncludeSearchPaths}
)
FIND_LIBRARY(LCM_LIBRARIES
  NAMES lcm
  PATHS ${LCM_LibrarySearchPaths}
)

SET(LCM_INCLUDE_DIR ${LCM_INCLUDE_DIR} ${CMAKE_BINARY_DIR}/${LCM_MSGS_DIR})

# Handle the REQUIRED argument and set the <UPPERCASED_NAME>_FOUND variable
FIND_PACKAGE_HANDLE_STANDARD_ARGS(LCM "Could NOT find LCM library (LCM)"
  LCM_LIBRARIES
  LCM_INCLUDE_DIR
)

MARK_AS_ADVANCED(
  LCM_INCLUDE_DIR
  LCM_LIBRARIES
)

function(lcm_generate_message LCM_MSG)
# Function to generate a .lcm message file
# only does cpp for now, DO the others as well

	if(${LCM_NUM_MESSAGES} EQUAL "0")
		# This creates the ${LCM_MSGS_DIR} folder...
		add_custom_command(OUTPUT ${CMAKE_BINARY_DIR}/${LCM_MSGS_DIR}/
				COMMAND mkdir -p ${LCM_MSGS_DIR}/.cache)
		
		# ...and a dummy target so that make clean works properly
		add_custom_target(lcm_msgs ALL
		DEPENDS ${CMAKE_BINARY_DIR}/${LCM_MSGS_DIR}/)


	endif()

	MATH(EXPR LCM_NUM_INCR "${LCM_NUM_MESSAGES}+1")
	set(LCM_NUM_MESSAGES ${LCM_NUM_INCR} PARENT_SCOPE)

	get_filename_component(MSG_GROUP ${LCM_MSG} NAME_WE)

	add_custom_command(OUTPUT ${CMAKE_BINARY_DIR}/${LCM_MSGS_DIR}/.cache/${MSG_GROUP}
			COMMAND mkdir -p ${LCM_MSGS_DIR}/.cache
			# all the crap below is to make it deal with white spaces in the path, but...there must be a better way
			COMMAND cd ${LCM_MSGS_DIR} && lcm-gen -x \"`echo \"${LCM_MSG}\" | sed 's/\\\\\\\\//g'`\" && touch .cache/${MSG_GROUP} 
			WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
			DEPENDS ${LCM_MSG})

	add_custom_target(__${MSG_GROUP}__ ALL
	DEPENDS ${CMAKE_BINARY_DIR}/${LCM_MSGS_DIR}/.cache/${MSG_GROUP})
	
endfunction()

