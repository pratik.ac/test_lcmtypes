cmake_minimum_required(VERSION 2.8 FATAL_ERROR)

project(test)

set(POD_NAME test)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake/")

include(cmake/pods.cmake)
include(cmake/lcmtypes.cmake)

lcmtypes_build()

add_subdirectory(src)

# add an uninstall target
add_custom_target(uninstall
    COMMAND ${CMAKE_COMMAND} -P ${CMAKE_MODULE_PATH}/uninstall.cmake
)
